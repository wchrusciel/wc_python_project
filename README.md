# Robot Framework

## Pliki (zadanie z dnia 15.04) z kodem napisanym w języku C, jego skompilowana wersja do postaci biblioteki o rozszerzeniu .so, skrypt Pythona z zaimportowanym CTypes, plik .robot z keywordami do komunikacji z biblioteką oraz przykładowymi test case'ami w Robot Frameworku znajdują się w: </br> **RobotFramework/CTypes/** </br> <ins>Jeden test case failuje, ponieważ pokazuje celowe niepoprawne zaimplementowanie funkcji w języku C.</ins>

</br>

## Pliki .robot dwóch test case'ów (zadań z dnia 14.04) wyeksportowanych z Katalona (a następnie zmodyfikowanych) znajdują się w: </br> **RobotFramework/KatalonRobotFrameworkPython/**

</br>

## Raporty z testów z dnia 13.04 znajdują się w: </br> **RobotFramework/demo_calculator/test_report_13_04**

</br>

# WC_Python_Project

## Wynik projektu końcowego (Ulubione rzeczowniki autora książki):

![image](wynik.png)

## Plik tekstowy z ulubionymi słowami autora w danej ksiażce:

![image](output.png)

## Wynik przeprowadzonych testów projektu końcowego:

![image](testy.png)

