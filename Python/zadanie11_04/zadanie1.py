class Osoba:
    def __init__(self, wiek, kasa):
        self.wiek = wiek
        self.kasa = kasa

    def __lt__(self, other):
        if self.wiek > other.wiek:
            if self.kasa > other.kasa:
                print("Im starszy tym bardziej zachalanny")
            elif self.kasa < other.kasa:
                print("Im mlodszy tym bardziej zachalanny")
            else:
                print("Wiek nie gra roli")
        elif self.wiek < other.wiek:
            if self.kasa > other.kasa:
                print("Im mlodszy tym bardziej zachalanny")
            elif self.kasa < other.kasa:
                print("Im starszy tym bardziej zachalanny")
            else:
                print("Wiek nie gra roli")
        else:
            print("Ten sam wiek")

    def __int__(self):
        return self.kasa


def main():
    osoba1 = Osoba(20, 3000)
    osoba2 = Osoba(23, 3000)
    osoba1 < osoba2
    print(int(osoba1))


main()
