import ReadData


class Gracz:
    def __init__(self, punkty, zycia):
        self.punkty = punkty
        self.zycia = zycia

    def rightAnswer(self):
        self.punkty += 10

    def wrongAnswer(self):
        self.zycia -= 1

    def getLifes(self):
        return self.zycia

    def getScore(self):
        return self.punkty


def Gra():
    runda = 1
    gracz = Gracz(0, 3)
    print("Witaj w Quiz'ie PANSTAWA-STOLICE!!!")
    print(f"Na poczatek gry masz {gracz.getLifes()} zycia, a twoja liczba punktow wynosi {gracz.getScore()}.\n")
    while gracz.getLifes() > 0:
        print(f"Runda {runda}")
        klucz = ReadData.ReadData()
        print("Podaj nazwe stolicy podanego panstwa:", klucz["kraj"])
        if input() == klucz["stolica"]:
            gracz.rightAnswer()
            print(f"Gratulacje!!! Zdobybasz punkty\nMasz {gracz.getLifes()} zycia i {gracz.getScore()} punktow.\n")
        else:
            gracz.wrongAnswer()
            print("Nie tym razem. Poprawna odpowiedz to:", klucz["stolica"],
                  f"\nTracisz jedno zycie.\nMasz {gracz.getLifes()} zycia i {gracz.getScore()} punktow.\n")
        runda += 1
    print(f"Koniec gry. Masz na swoim koncie {gracz.getScore()} punktow.\n")


Gra()
