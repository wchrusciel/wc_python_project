import requests
import random

if __name__ == "__main__":
    print("File ReadData excecuted when run directly")


def ReadData():
    countryNumber = random.randint(0, 199)
    try:
        response = requests.get("https://geographyfieldwork.com/WorldCapitalCities.htm")
        if response.status_code != 200:
            raise Exception
    except Exception:
        print("Access denied")

    textFromResponse = response.text

    beginningOfList = textFromResponse.find("Afghanistan")
    endOfList = textFromResponse.find("Harare")
    textFromResponse = textFromResponse[beginningOfList - 22:endOfList + 18]

    while countryNumber != 0:
        textFromResponse = textFromResponse[textFromResponse.find("</tr>") + 7:]
        countryNumber -= 1

    countryBegin = textFromResponse.find("<tr>") + 22

    capitalEnd = textFromResponse.find("</tr>") - 2

    textFromResponse = textFromResponse[countryBegin:capitalEnd]
    country = textFromResponse[:textFromResponse.find("<")]
    textFromResponse = textFromResponse[textFromResponse.find('17">') + 4:]
    capital = textFromResponse[:textFromResponse.find("<")]

    return dict(kraj=country, stolica=capital)


