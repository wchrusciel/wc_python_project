import random

plec = ['m', 'f']


class Niemiec:
    def __init__(self, wiek, plec):
        self.wiek = wiek
        # self.imie = imie
        self.plec = plec

    def __mul__(self, other):
        return Niemiec(0, random.choice(plec))

    def __str__(self):
        return 'wiek:' + str(self.wiek) + ' plec:' + self.plec


niemiec1 = Niemiec(13, 'm')
niemiec2 = Niemiec(15, 'f')
try:
    niemiec3 = niemiec1 * niemiec2
    if niemiec1.wiek <= 14 and niemiec2.wiek <= 14:
        raise Exception
    if (16 <= niemiec1.wiek and 16 > niemiec2.wiek) or (16 <= niemiec2.wiek and 16 > niemiec1.wiek):
        raise Exception
except Exception:
    print("Przestepstwo")
if niemiec3:
    print(niemiec3)
