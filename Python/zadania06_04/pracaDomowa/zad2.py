# Funkcja wyswietlajca choinke z gwiazdek dla zadanego n
# Author: W. Chrusciel
def choineczka(n):
    i = n
    while n > 0:
        print('*'*n)
        n -= 1
    n += 1
    while n +1 <= i:
        print('*' * (n+1))
        n += 1   


choineczka(3)
