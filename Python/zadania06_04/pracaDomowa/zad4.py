# Funkcja obliczająca zapotrzebowanie kaloryczne wg wzoru Harrisa-Benedicta
# Author: W. Chrusciel

def BMR(sex, weight, height, age):
    if sex == "male":
        return 66.47+13.7*weight+5*height-6.76*age
    elif sex == "female":
        return 655.1 + 9.567 * weight + 1.85 * height - 4.68 * age
    else:
        return "There's only male and female"

print(BMR("female",80,180,22))
