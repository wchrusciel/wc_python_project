# Skrypt tworzący tablice 100 liczb pierwszych
# Author: W. Chrusciel

# Funkcja sprawdzająca czy podany argument jest liczbą pierwszą

def isPrime(x):
    if x < 2:
        return False

    for i in range(3,int(x**0.5)+1,2):
        if x % i == 0:
            return False
    return True

# Funkcja tworząca tablice 100 liczb pierwszych

def createTable():
    table = []
    i = 0
    size = 100;
    while size > 0:
        if isPrime(i) == True:
            table.append(i)
            
            size = size - 1
        i = i + 1
    print(table)

createTable()
        
