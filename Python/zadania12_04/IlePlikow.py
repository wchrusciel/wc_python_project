import os
counter = 0
size = 0
for folderName, subfolders, filenames in os.walk(r"c:\Windows"):
    for f in filenames:
        try:
            if f[len(f)-4:] == ".xml":
                counter += 1
                size += os.path.getsize(folderName+'/'+f)
        except:
            pass
with open("output", 'w') as f:
    f.write(f"number of files:{counter} total size: {round(size/100000,2)}MB\n")
    f.close()