import unittest
from books import Book


class TestBooks(unittest.TestCase):
    def setUp(self):
        self.book = Book("Victory.txt")

    def test_sanitaze_word(self):
        self.assertEqual("lord", self.book.sanitize("'Lord"))

    def test_sanitaze_word_int(self):
        self.assertRaises(TypeError, self.book.sanitize(123))

    def test_read_file_checkString(self):
        self.assertEqual(str, type(list(self.book.read_file().keys())[0]))

    def test_read_file_checkInt(self):
        self.assertEqual(int, type(list(self.book.read_file().values())[0]))


if __name__ == "__main__":
    unittest.main()