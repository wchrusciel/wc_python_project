import nltk
from nltk.corpus import wordnet as wn
import threading
import re

nltk.download('wordnet')
nltk.download('omw-1.4')
nouns = {x.name().split('.', 1)[0] for x in wn.all_synsets('n')}

tables = []

class Book:
    def __init__(self, name):
        self.filename = name
        self.name = name.strip('.txt')
        
        self.words = self.read_file()
    
        self.top_words = [[x, self.words[x]] for x in list(self.words)[0:10]]

        filtered_words = [x for x in self.words if len(str(x)) > 2]
        self.top_nouns = [[x, self.words[x]] for x in list(filtered_words) if x in nouns][0:10]

        self.table = self.generate_table()
        tables.append(self.table)

    def sanitize(self, word):
        word = str(word)
        word = re.sub(r'[^\w\s]','', word)
        word = word.lower()
        return str(word)
    
    def read_file(self):
        words = {}
        with open(self.filename) as f:
            for line in f:
                for word in line.split():
                    corrected_word = self.sanitize(word)
                    if corrected_word in words:
                        words[corrected_word] += 1
                    else:
                        words[corrected_word] = 1
        return dict(sorted(words.items(), key=lambda x: x[1], reverse=True))

    def generate_table(self):
        ret = f"{self.name}:\n\n"
        ret += "all words:\n"
        i = 1
        for key in self.top_words:
            w = str('"' + key[0] + '"').ljust(10)
            ret += f'{str(i).zfill(2)}.  {w} {key[1]}\n'
            i += 1
            
        ret += "\nonly nouns:\n"
        i = 1
        for key in self.top_nouns:
                    w = str('"' + key[0] + '"').ljust(10)
                    ret += f'{str(i).zfill(2)}.  {w} {key[1]}\n'
                    i += 1
                    
        return ret


if __name__ == "__main__":
    
    books = ["Lord Jim.txt",
             "Victory.txt", 
             "A Tale of the Seaboard.txt",
             "Heart of Darkness.txt",
             "The Nigger Of The Narcissus.txt"]
             
    for x in range(5):
        thread = threading.Thread(target=Book, args=(books[x],))
        thread.start()

    while len(tables) < 5:
        pass
        
    with open('output', 'w') as f:
        for table in tables:
            print(table)
            f.write(table)      