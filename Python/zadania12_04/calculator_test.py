import unittest
from calculator import Calculate


class TestCalculate(unittest.TestCase):
    def setUp(self):
        self.calc = Calculate()

    def test_add_method_with_small_number(self):
        self.assertEqual(4, self.calc.add(2, 2))

    def test_add_big_number(self):
        self.assertEqual(2_000_000,
                         self.calc.add(1_000_000, 1_000_000))


if __name__ == "__main__":
    unittest.main()
