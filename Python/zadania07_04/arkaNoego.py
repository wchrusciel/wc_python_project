class Zwierze():
    def daj_glos(self):
        print("Jestem zwierzeciem")

class Los(Zwierze):
    def daj_glos(self):
        print("Muu!")

class Kon(Zwierze):
    def daj_glos(self):
        print("Brrrrr!")

class Pies(Zwierze):
    def daj_glos(self):
        print("Hau hau!")

class Kura(Zwierze):
    def daj_glos(self):
        print("Ko ko!")

class Kaczka(Zwierze):
    def daj_glos(self):
        print("Kwa kwa!")

class Bird(Zwierze):
    def jem(self):
        print("Jem ziarno")

class Dog(Zwierze):
    def jem(self):
        print("Jem mieso")

class Ptakopies(Bird,Dog):
    pass

los1 = Los()
los2 = Los()
kon1 = Kon()
kon2 = Kon()
pies1 = Pies()
pies2 = Pies()
kura1 = Kura()
kura2 = Kura()
kaczka1 = Kaczka()
kaczka2 = Kaczka()
ptakopies1 = Ptakopies()



arkaNoego = [los1, los2, kon1, kon2, pies1, pies2, kura1, kura2, kaczka1, kaczka2, ptakopies1]

for i in arkaNoego:
    i.daj_glos()