class Animal():
    def jem(self):
        print("Jem jedzenie")


class Bird(Animal):
    def jem(self):
        print("Jem ziarno")

class Kot(Animal):
    def __init__(self, kolor = "czarne"):
        self.kolor = kolor
        self.ilosc_nog = 4
        self.ilosc_zyc = 9
    def jem(self):
        print("Jem mieso")
    def wybiel(self):
        self.kolor = "bialy"
        self.ilosc_zyc -= 1

class Ptakokot(Bird,Kot):
    pass

miaurycy = Kot("black")
#miaurycy.kolor = "bialy"
miaurycy.ilosc_zyc -= 1
miaurycy.wybiel()

cyryl = Kot()

print(miaurycy.kolor)

