import random
from enum import Enum


class Colour(Enum):
    KIER = "♥"
    KARO = "♢"
    TREFL = "♣"
    PIK = "♤"


class Value(Enum):
    TWO = [2, '2']
    THREE = [3, '3']
    FOUR = [4, '4']
    FIVE = [5, '5']
    SIX = [6, '6']
    SEVEN = [7, '7']
    EIGHT = [8, '8']
    NINE = [9, '9']
    TEN = [10, '10']
    JACK = [11, 'J']
    QUEEN = [12, 'Q']
    KING = [13, 'K']
    ACE = [14, "A"]


class Card:
    def __init__(self, colour, value):
        self.kolor = colour
        self.wartosc = value

    def cardValue(self):
        return self.wartosc.value[0]

    def __str__(self):
        return self.kolor.value + self.wartosc.value[1]


class Deck:
    def __init__(self, low_bound=Value.ACE, tab=[]):
        if len(tab) == 0:
            self.pile = []
            for i in Colour:
                for j in Value:
                    if int(j.value[0]) >= low_bound.value[0]:  # Append to pile only cards higher than low_bound.value
                        self.pile.append(Card(i, j))
        else:
            self.pile = tab

    def shuffle(self):
        random.shuffle(self.pile)

    def split(self):
        temp = self.pile[int(len(self.pile) / 2):]
        self.pile = self.pile[:int(len(self.pile) / 2)]
        return temp

    def cardInDeckValue(self, index):
        return self.pile[index].cardValue()

    def __str__(self):
        temp = ""
        for i in self.pile:
            temp += str(i) + "\t"
        return temp


class Player:
    def __init__(self, name, pile):
        self.name = name
        self.playerPile = pile

    def removeLastCard(self):
        return self.playerPile.pile.pop(self.pileSize() - 1)

    def pileSize(self):
        return len(self.playerPile.pile)


def singleRound(player1, player2, war_pile=[]):
    cards_on_table = war_pile
    player1Card = player1.removeLastCard()
    player2Card = player2.removeLastCard()
    cards_on_table.append(player1Card)
    cards_on_table.append(player2Card)
    random.shuffle(cards_on_table)
    if player1Card.cardValue() > player2Card.cardValue():
        player1.playerPile.pile = cards_on_table + player1.playerPile.pile
    elif player1Card.cardValue() < player2Card.cardValue():
        player2.playerPile.pile = cards_on_table + player2.playerPile.pile
    else:
        if player1.pileSize() < 2 or player2.pileSize() < 2:  # condition for less than 3 card on pile in case of war
            return True
        player1Card = player1.removeLastCard()
        player2Card = player2.removeLastCard()
        cards_on_table.append(player1Card)
        cards_on_table.append(player2Card)
        singleRound(player1, player2, cards_on_table)  # recursively calling singleRound in case of war situation
    return False


def game():
    turn = 1  # turn counter
    low_bound = Value.TWO
    talia = Deck(low_bound)
    talia.shuffle()
    player1 = Player(input("Podaj imie pierwszego gracza: "), Deck(low_bound, talia.split()))
    player2 = Player(input("Podaj imie drugiego gracza: "), talia)

    print("Tura: ", turn)
    print("Karty", player1.name, " \t:", player1.playerPile)
    print("Karty", player2.name, " \t:", player2.playerPile)
    print("Il. kart", player1.name, "\t:", player1.pileSize())
    print("Il. kart", player2.name, "\t:", player2.pileSize(), "\n")
    turn += 1
    while player1.pileSize() > 0 and player2.pileSize() > 0:
        if singleRound(player1, player2, []):
            break
        print("Tura: ", turn)
        print("Karty", player1.name, " \t:", player1.playerPile)
        print("Karty", player2.name, " \t:", player2.playerPile)
        print("Il. kart", player1.name, "\t:", player1.pileSize())
        print("Il. kart", player2.name, "\t:", player2.pileSize(), "\n")
        turn += 1

    if player1.pileSize() > player2.pileSize():
        print("Wygrał/a ", player1.name, "!!!")
    else:
        print("Wygrał/a ", player2.name, "!!!")


game()
