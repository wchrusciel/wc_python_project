class Point:
    def __init__(self, x=0, y=0, z=0, u=0, w=0):
        self.set(x, y, z, u, w)

    def set(self, x, y, z, u, w):
        self.x = x
        self.y = y
        self.z = z
        self.u = u
        self.w = w

    def reset(self):
        self.set(0, 0, 0, 0, 0)

    def modulus(self):
        return (self.x ** 2 + self.y ** 2 + self.z ** 2 + self.u ** 2 + self.w ** 2) ** (0.5)


def distance(punkt1, punkt2):
    return ((punkt1.x - punkt2.x) ** 2 + (punkt1.y - punkt2.y) ** 2 + (punkt1.z - punkt2.z) ** 2 + (
                punkt1.u - punkt2.u) ** 2 + (punkt1.w - punkt2.w) ** 2) ** 0.5


point1 = Point(3, 1, 2, 2, 3)
point2 = Point(3, 3, 5, 2, 7)
print("Modul pierwszego punktu:   ", point1.modulus())
print("Modul pierwszego punktu:   ", point2.modulus())
print("Odleglosc miedzy punktami: ", distance(point1, point2))
