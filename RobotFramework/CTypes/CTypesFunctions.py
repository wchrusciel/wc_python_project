import ctypes

_functions = ctypes.CDLL('C:\DANE\Szkolenie\Python\wc_python_project\RobotFramework\CTypes\myclib.so')
_functions.mean_function.argtypes = (ctypes.c_int, ctypes.POINTER(ctypes.c_int))
_functions.sum_function.argtypes = (ctypes.c_int, ctypes.POINTER(ctypes.c_int))
_functions.high_function.argtypes = (ctypes.c_int, ctypes.POINTER(ctypes.c_int))

def strip_list(expression):
    expression = expression.split()
    new_expression = [int(x) for x in expression]
    return new_expression


def high_function(numbers):
    global _functions
    num_numbers = len(numbers)
    array_type = ctypes.c_int * num_numbers
    result = _functions.high_function(ctypes.c_int(num_numbers), array_type(*numbers))
    return result


def mean_function(numbers):
    global _functions
    num_numbers = len(numbers)
    array_type = ctypes.c_int * num_numbers
    result = _functions.mean_function(ctypes.c_int(num_numbers), array_type(*numbers))
    return float(result)


def sum_function(numbers):
    global _functions
    num_numbers = len(numbers)
    array_type = ctypes.c_int * num_numbers
    result = _functions.sum_function(ctypes.c_int(num_numbers), array_type(*numbers))
    return result