int sum_function(int num_numbers, int *numbers)
{
        int i;
        int sum = 0;
        for (i = 0; i < num_numbers; i++) {
                sum += numbers[i];
        }
        return sum;
}

int high_function(int num_numbers, int *numbers)
{
        int i;
        int high = numbers[0];
        for (i = 1; i < num_numbers; i++) {
                if (numbers[i] > high) {
                        high = numbers[i];
                }
        }
        return high;
}

int mean_function(int num_numbers, int *numbers)
{
        int i;
        int sum = 0;
        for (i = 0; i < num_numbers; i++) {
                sum += numbers[i];
        }
        return (float)sum/num_numbers;
}