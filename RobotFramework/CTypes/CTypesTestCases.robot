*** Settings ***
Documentation		Example test cases for functions preated 
...					in Python with used CTypes
...					- high_function - returns biggest element in list
...					- mean_function - returns artithemtic mean from list elements
...					- sum_function - returns sum of list elements
...					
...					sum_funtion was written incorrectly to show that
...					some test cases will fail	
Library  CTypesFunctions.py
Library  Collections

*** Variables ***
${LIST}
${RESULT}

*** Test Cases ***
Highest element
	Given input list elements is  6 2 3 36 19 25
	When user uses high function
	Then result is  36

Addition element
	Given input list elements is  1 2 3 4 5
	When user uses sum function
	Then result is  15
	
Arithmetical mean int result
	Given input list elements is  1 3 5 7
	When user uses mean function
	Then result is  4

Arithmetical mean float result
	Given input list elements is  1 2 3 4
	When user uses mean function
	Then result is  2.5
	
*** Keywords ***
Input list elements is
	[Arguments]     ${expression}
	${LIST}=  STRIP LIST  ${expression}
	Set Suite Variable  ${LIST}
	
User uses high function
	${RESULT}=  high function  ${LIST}
	Set Suite Variable  ${RESULT}
	
User uses sum function
	${RESULT}=  sum function  ${LIST}
	Set Suite Variable  ${RESULT}
		
User uses mean function
	${RESULT}=  mean function  ${LIST}
	Set Suite Variable  ${RESULT}
	
Result is
	[Arguments]     ${expectedValue}
	assertValue  ${RESULT}  ${expectedValue}

assertValue
    [Arguments]                  ${expression}  ${expectedValue}
    Should Be Equal As Numbers   ${expression}  ${expectedValue}