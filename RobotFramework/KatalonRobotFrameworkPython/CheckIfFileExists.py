from os.path import exists

#from robot.api.deco import keyword


#@keyword("CUSTOM KEYWORD TO CHECK FILE")
def check_if_file_exists(filename):
    file_exists = exists(filename)
    return file_exists
